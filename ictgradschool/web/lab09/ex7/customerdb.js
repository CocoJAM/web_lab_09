var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];


var number_M = 0;
var number_F = 0;
var greaterthan65= 0;
var greaterthan31= 0;
var greaterthan0= 0;
var gold = 0;
var	silver = 0;
var	bronze = 0;
var date = new Date;
var year = date.getFullYear();
var list_of_content = ["name", "gender", "year_born", "joined", "num_hires"];
var table_content = document.createElement("table");
var to_body = document.getElementsByTagName("body")[0];
to_body.appendChild(table_content);
for(var i=0 ; i<list_of_content.length; i++){
	table_content.insertRow(i).id = i;
    document.getElementsByTagName("tr")[i].innerHTML = list_of_content[i];
    document.getElementsByTagName("tr")[i].style.border = "solid";
    for(var j=0 ; j<customers.length; j++){
        // var item = list_of_content[i];
        // console.log(item);
        // document.getElementsByTagName("tr")[i].appendChild(document.createElement("td")).innerHTML = customers[j].name;
		if(i==0){
        document.getElementsByTagName("tr")[i].appendChild(document.createElement("td")).innerHTML = customers[j].name;}
        if(i==1){
        document.getElementsByTagName("tr")[i].appendChild(document.createElement("td")).innerHTML = customers[j].gender;}
            if(i==2){
        document.getElementsByTagName("tr")[i].appendChild(document.createElement("td")).innerHTML = customers[j].year_born;}
                if(i==3){
        document.getElementsByTagName("tr")[i].appendChild(document.createElement("td")).innerHTML = customers[j].joined;}
                    if(i==4){
        document.getElementsByTagName("tr")[i].appendChild(document.createElement("td")).innerHTML = customers[j].num_hires;}
    }
}


for(var i =0 ;i<customers.length; i++){

	if(customers[i].gender == 'male'){number_M++;}
	if (customers[i].gender == 'female'){number_F++;}
	if( year- customers[i].year_born> 64){
		greaterthan65++;
	}
	if ( year- customers[i].year_born< 65 &&year- customers[i].year_born> 30){
        greaterthan31++;
	}
	if ( year- customers[i].year_born< 31 &&year- customers[i].year_born> -1){
        greaterthan0++;}
	if(customers[i].num_hires/(year- customers[i].year_born)/52> 4){
		gold++;
	}
	if (customers[i].num_hires/(year- customers[i].year_born)/52< 4&& customers[i].num_hires/(year- customers[i].year_born)/52> 0){
		silver++;
	}
    if (customers[i].num_hires/(year- customers[i].year_born)/52< 2&& customers[i].num_hires/(year- customers[i].year_born)/52> -1){
        bronze++;
    }
}
var list = [number_M,number_F,greaterthan65,greaterthan31,greaterthan0,gold,silver,bronze];
var list_name = ["males", "females", "age range 65+", "age range from 31-64", "age range from 0-30", "gold", "sliver", "bronze"];

for(var i= 0 ; i <list.length ; i++){
    var para = document.createElement("p");
    para.innerHTML = "There are " +  list[i] + " of " + list_name[i] ;
    to_body.appendChild(para);
}


// for (var i= 0 ; i <list.length ; i++){
//     var creationTr = document.createElement("tr");
//     var creationTd = document.createElement("td");
//     var creationtd = document.createElement("td");
//     creationTd.innerHTML = list[i];
//     creationtd.innerHTML = list_name[i];
//     var tableContent = creationTr.appendChild(creationtd);
//     tableContent = creationTr.appendChild(creationTd);
//     table.appendChild(creationTr);
//
// }